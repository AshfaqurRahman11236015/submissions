package exercise3.id11236015.com.exercise3;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MyActivity extends ActionBarActivity {

    private EditText txtSubjName;
    private EditText txtSubjNumber;
    private Button btnRotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        txtSubjName = (EditText)findViewById(R.id.txtSubjName);
        txtSubjName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String onFocus = "Subject name HAS focus";
                String lostFocus = "Subject name LOST focus";
                if(hasFocus) {
                    Toast.makeText(getBaseContext(), onFocus, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getBaseContext(), lostFocus, Toast.LENGTH_SHORT).show();
                }
            }
        });

        txtSubjNumber = (EditText)findViewById(R.id.txtSubjNumber);
        btnRotate = (Button)findViewById(R.id.btnRotate);
        btnRotate.setOnClickListener(btnRotateListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("SubjName",txtSubjName.getText().toString());
        outState.putString("SubjNumber",txtSubjNumber.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String subjName = savedInstanceState.getString("SubjName");
        String subjNumber = savedInstanceState.getString("SubjNumber");
        txtSubjName.setText(subjName);
        txtSubjNumber.setText(subjNumber);
    }

    private View.OnClickListener btnRotateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int current = getResources().getConfiguration().orientation;

            if(current == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    };
}

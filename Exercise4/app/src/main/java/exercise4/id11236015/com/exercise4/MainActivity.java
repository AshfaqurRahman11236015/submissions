package exercise4.id11236015.com.exercise4;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private final int mMax = 5;
    private final String mProgressMessage = "Downloading...";
    private Handler handler = new Handler();
    private ProgressDialog mProgress;
    private Spinner spinArtists;
    private int progressStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinArtists = (Spinner)findViewById(R.id.spinArtists);
        spinArtists.setOnItemSelectedListener(spinListener);
        mProgress = new ProgressDialog(this);
    }

    public void download1(View v) {
        showProgressDialog();
        progressStatus = 0;
        final Thread t = new Thread() {
            @Override
            public void run() {
                while(progressStatus < mMax) {
                    try {
                        Thread.sleep(1000);
                        progressStatus += 1;
                        mProgress.incrementProgressBy(progressStatus);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                mProgress.dismiss();
            }
        };
        t.start();
    }

    public void download2(View v) {
        showProgressDialog();

        progressStatus = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(progressStatus < mMax) {
                    progressStatus += 1;

                    try {
                        Thread.sleep(1000);
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mProgress.setProgress(progressStatus);
                        }
                    });
                }
                mProgress.dismiss();
            }
        }).start();
    }

    private void showProgressDialog() {
        mProgress.setMessage(mProgressMessage);
        mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgress.setProgress(0);
        mProgress.setMax(mMax);
        mProgress.show();
    }

    private AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String[] artists = getResources().getStringArray(R.array.artists);
            Toast.makeText(getBaseContext(),artists[position],Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.id11236015.exercise6;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Iterator;
import java.util.Set;

public class MainActivity extends Activity {

    private static final String LAB_PREF = "LAB_PREF";
    private TextView tvFavouriteSubject;
    private TextView tvLabTime;
    private TextView tvHandbookURL;
    private TextView tvReminderRingtone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvFavouriteSubject = (TextView)findViewById(R.id.tvFavSubj);
        tvLabTime = (TextView)findViewById(R.id.tvLabTime);
        tvHandbookURL = (TextView)findViewById(R.id.tvHandbookURL);
        tvReminderRingtone = (TextView)findViewById(R.id.tvRemindRingtone);

        RestoreUIState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        PreferenceMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return MenuChoice(item);
    }

    // Creating the Preference Menu
    private void PreferenceMenu(Menu menu) {
        MenuItem preferenceMenu = menu.add(0,0,0,"Preference");
        {
            preferenceMenu.setIcon(R.drawable.gear);
            preferenceMenu.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
    }

    // Preference Menu On Click function
    private boolean MenuChoice(MenuItem item) {
        switch(item.getItemId()) {
            case 0:
                startActivity(new Intent(this, MyPreferenceActivity.class));
                return true;
        }
        return false;
    }

    private void RestoreUIState() {
        PreferenceManager.setDefaultValues(getBaseContext(), R.xml.pref_preferences, false);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String strFavouriteSubject = sp.getString("prefSubjects", null);

        String strLabTimes = "";
        Set<String> selections = sp.getStringSet("prefLabTimes", null);

        Iterator iterator = selections.iterator();
        while(iterator.hasNext()) {
            strLabTimes += (String) iterator.next();
            if(iterator.hasNext() == true) {
                strLabTimes += ", ";
            }
        }

        String strHandbookURL = sp.getString("prefUTSHandbookURL", null);
        String strRingtone = sp.getString("prefRingtone", null);

        tvFavouriteSubject.setText(strFavouriteSubject);
        tvLabTime.setText(strLabTimes);
        tvHandbookURL.setText(strHandbookURL);
        tvReminderRingtone.setText(strRingtone);
    }
}

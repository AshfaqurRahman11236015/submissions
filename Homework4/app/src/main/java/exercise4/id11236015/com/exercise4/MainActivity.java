package exercise4.id11236015.com.exercise4;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private final int mMax = 5;
    private final String mProgressMessage = "Downloading...";
    private Handler handler = new Handler();
    private ProgressDialog mProgress;
    private Spinner spinArtists;
    private int progressStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinArtists = (Spinner)findViewById(R.id.spinArtists);
        spinArtists.setOnItemSelectedListener(spinListener);
    }

    // Download 1 method using a thread for progress dialog
    public void download1(View view) {
        showProgressDialog();
        progressStatus = 0;
        final Thread t = new Thread() {
            @Override
            public void run() {
                while(progressStatus < mMax) {
                    try {
                        Thread.sleep(1000);
                        progressStatus += 1;
                        mProgress.incrementProgressBy(progressStatus);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                mProgress.dismiss();
            }
        };
        t.start();
    }

    // Download 2 button method using a handler
    public void download2(View view) {
        showProgressDialog();

        progressStatus = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(progressStatus < mMax) {
                    progressStatus += 1;

                    try {
                        Thread.sleep(1000);
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mProgress.setProgress(progressStatus);
                        }
                    });
                }
                mProgress.dismiss();
            }
        }).start();
    }

    public void download3(View view) {
        showProgressDialog();

        // initializing the MyAsyncTask
        new MyAsyncTask(200).execute(2,1000);
    }

    private void showProgressDialog() {
        // Setting up the Progress Dialog box variables such as
        // progress box style, maximum value and the message to be
        // displayed in the dialog box
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(mProgressMessage);
        mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgress.setProgress(0);
        mProgress.setMax(mMax);
        mProgress.show();
    }

    private AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // retrieving all the artists string array
            String[] artists = getResources().getStringArray(R.array.artists);
            // based on the selected position of the spinner list the artist name
            // is selected from the artists array
            Toast.makeText(getBaseContext(),artists[position],Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * MyAsyncTask dedicated to the progress bar dialog which takes 2 integer parameters
     * and sets a void method which will run the thread.
     */
    private class MyAsyncTask extends AsyncTask<Integer, Integer, Void> {
        private int mMaxVal;
        private int mIncrement;
        private int mProgressVal;

        protected MyAsyncTask(int max) {
            // setting the initial values
            // taking a max integer as a parameter
            mMaxVal = max;
            mIncrement = 0;
            mProgressVal = 0;
        }

        @Override
        protected Void doInBackground(Integer... params) {
            // get the incremental value from the integer parameter (1st integer value)
            mIncrement = params[0];
            mProgress.setMax(mMaxVal);

            // while the progress value is less than max value
            // this function will be run
            while(mProgressVal < mMaxVal) {
                mProgressVal += mIncrement;
                try {
                    // the time between each increment is set here
                    // from the given parameter (2nd integer value)
                    Thread.sleep(params[1]);
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(mProgressVal);
            }
            return null;
        }

        // this method is called to update the progress bar
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mProgress.setProgress(values[0]);
        }

        // to conclude the async task this method is called
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgress.dismiss();
        }
    }
}

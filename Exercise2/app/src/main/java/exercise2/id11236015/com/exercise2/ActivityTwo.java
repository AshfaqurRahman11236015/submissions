package exercise2.id11236015.com.exercise2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import exercise2.id11236015.com.exercise2.R;

public class ActivityTwo extends ActionBarActivity {

    private TextView tvSubjName;
    private TextView tvSubjNumber;
    private TextView tvSubjType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        tvSubjName = (TextView)findViewById(R.id.tvSubjName);
        tvSubjNumber = (TextView)findViewById(R.id.tvSubjNumber);
        tvSubjType = (TextView)findViewById(R.id.tvSubjType);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            tvSubjName.setText(extras.getString("SubjName"));
            tvSubjNumber.setText(extras.getString("SubjNumber"));
            tvSubjType.setText(extras.getString("SubjType"));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_two, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showPopup(View view) {

        String message = tvSubjName.getText().toString() + '\n' +
                         tvSubjNumber.getText().toString() + '\n' +
                         tvSubjType.getText().toString();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Subject");
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}

package exercise2.id11236015.com.exercise2;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

public class ActivityOne extends ActionBarActivity {

    private EditText txtSubjName;
    private EditText txtSubjNumber;
    private RadioGroup rgSubjType;
    private Button btnShowA1;
    private Button btnShowA2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        txtSubjName = (EditText)findViewById(R.id.txtSubjName);
        txtSubjNumber = (EditText)findViewById(R.id.txtSubjNumber);
        rgSubjType = (RadioGroup)findViewById(R.id.rgSubjType);
        btnShowA1 = (Button)findViewById(R.id.btnShowA1);
        btnShowA2 = (Button)findViewById(R.id.btnShowA2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean valid() {
        String error = "";
        int duration = Toast.LENGTH_LONG;

        if(txtSubjName.length() == 0) {
            error = "No input for subject name";
            Toast.makeText(this, error, duration).show();
            return false;
        } else if (txtSubjNumber.length() == 0) {
            error = "No input for subject number";
            Toast.makeText(this, error, duration).show();
            return false;
        }
        return true;
    }

    public void showDetails(View view) {
        btnShowA1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valid()) {
                    int selectedId = rgSubjType.getCheckedRadioButtonId();
                    RadioButton rdSelected = (RadioButton)findViewById(selectedId);
                    Intent intent = new Intent(getApplicationContext(), ActivityTwo.class);
                    intent.putExtra("SubjName", txtSubjName.getText().toString());
                    intent.putExtra("SubjNumber", txtSubjNumber.getText().toString());
                    intent.putExtra("SubjType", rdSelected.getText().toString());
                    startActivity(intent);
                }
            }
        });
    }

    public void showA2(View view) {
        btnShowA2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityThree.class);
                startActivityForResult(intent, Constants.REQUEST_CODE);
            }
        });
    }
}
